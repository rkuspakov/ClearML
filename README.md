## Первичная синхронизация с GitLab (Возможные команды по инициализации и синхронизации)
git init
git reset
git push --set-upstream origin main
git fetch  
git push -u

# Подготовка к настройке

## Скачиваем тестовые данные с сайта
https://www.kaggle.com/datasets/kritanjalijain/amazon-reviews?resource=download&select=train.csv и помещаем в папку ClearML\data\train.csv

## Переходим в папку
cd .\infra

## Запускаем команду
docer-compose up

## Открываем второй терминал и устанавливаем
pip install clearml

## Инициализируем сессию
clearml-init

## Переходим по ссылке
http://localhost:8080//settings/workspace-configuration и вводим имя и фамилию

## Создаем новые данные: Access key и Secret key
            api {
            web_server:http://localhost:8080/
            api_server:http://localhost:8008
            files_server:http://localhost:8081
            credentials {
                        "access_key"="1V3OI2M6MTPOMPG282UEO02AV61677"
                        "secret_key"="1GUsYk4231lHcL9lORA_1DTc5Nz2R6tRjLR8Bkjm20WMIW5bvYImzR9GHlk12Cajnpg"
                        }
            }

            Вводим эти данные в окно запроса в терминале или создаем файл: C:\Users\'Name User'\clearml.conf

## Загрузим данные
clearml-data create --project "Amazon reviews" --name "Raw data"

## После создания dataset перейдем по ссылке
http://localhost:8080/datasets/simple/9dec5fac8a46406dbadb6ccefaf91a08/experiments/e80d359b1c1e4e6c81ca6603e80fb38a?columns=selected&columns=name&columns=hyperparams.properties.version&columns=tags&columns=status&columns=project.name&columns=users&columns=started&columns=last_update&order=-last_update&filter=

И увидим, что создался пустой dataset

## Переходим в терминальное окно и выполняем команду
1. Подготовка: clearml-data add --files data/train.csv
2. Загрузка: clearml-data upload
3. Закрытие: clearml-data close

***Завершена подготовка к выполнению кода***

# Выполнение кода

## Выполняем команду:
pip install click, joblib, polars, clearml, scikit-learn, nltk
python test_mlops

## Можем посмотреть данные на сайте
http://localhost:8080/datasets/simple/9dec5fac8a46406dbadb6ccefaf91a08/experiments/e80d359b1c1e4e6c81ca6603e80fb38a?columns=selected&columns=name&columns=hyperparams.properties.version&columns=tags&columns=status&columns=project.name&columns=users&columns=started&columns=last_update&order=-last_update&filter=

## Устанавливаем агент:
pip install clearml-agent

## Отредактируем файл настройки:
nano ~/clearml.conf и заполняем данные файла

## Выполняем команду в терминале:
clearml-agent daemon --detached --queue default
(Для получения дополнительной информации об очереди)

## После завершение эксперимента можем перейти на сайт и просмотреть результаты его выполнения

## Можно скопировать эксперимент, изменить параметры и заново запустить его с новыми параметрами
Доступен просмотр, выбор результатов сравнения и т.д. Создание отчетов и все эксперименты и данные по ним

## Запуск в Jupyter Notebook
Открываем блокнот bert_amazon.ipynb и запускаем на выполнение все ячейки


## Просмотр загрузки видеокарты (Для скилов)
watch -n 1 nvidia-smi









https://clear.ml/docs/latest/docs/getting_started/ds/ds_first_steps/